#include <string.h>
#include <stdio.h>

#include "contiki.h"

#include "ECC.h"
#include "NN.h"
#include "sha1.h"
#include "CurveParam.h"



//Private Keys
static NN_DIGIT msk[NUMWORDS];           //msk
static NN_DIGIT SecretKeyA[NUMWORDS];    //x_A
static NN_DIGIT SecretKeyB[NUMWORDS];    //x_B
  
static NN_DIGIT SecretKGC[NUMWORDS];     //r_A or r_B
static NN_DIGIT PartialPrivA[NUMWORDS];  //d_A
static NN_DIGIT PartialPrivB[NUMWORDS];  //d_B
static NN_DIGIT FullPrivA[NUMWORDS];     //sk_A
static NN_DIGIT FullPrivB[NUMWORDS];     //sk_B

static NN_DIGIT l_A[NUMWORDS];           //l_A
static NN_DIGIT T[NUMWORDS];             //T
static NN_DIGIT W[NUMWORDS];             //W

//Shared Symmetric Session Keys
static uint8_t SecretK1[KEYDIGITS*NN_DIGIT_LEN];     //K1
static uint8_t SecretK2[KEYDIGITS*NN_DIGIT_LEN];     //K2    K1 = K2 for successful decapsulation
  

//Public Keys
Point PubKeyKGC;                  //P_PUB
Point SecPubKeyA;                 //P_A
Point SecPubKeyB;                 //P_B

Point PartialPubKGC;              //R_A
Point FullPubKeyA;                //pk_A
Point FullPubKeyB;                //pk_B

Point U;                          //U

static uint32_t t = 0;
static uint8_t A_ID = 1;
static uint8_t B_ID = 2;

static int round_index;
#define MAX_ROUNDS 10

static Params * param;


/* declaration of scopes process */
PROCESS(fitf_process, "FitF Experiment Process");
AUTOSTART_PROCESSES(&fitf_process);

static void init_data(){
    //uint32_t time_a, time_b;
	
    //initialize all private keys to zero
    memset(msk, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(SecretKeyA, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(SecretKeyB, 0, NUMWORDS*NN_DIGIT_LEN);

    memset(SecretKGC, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(FullPrivA, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(FullPrivB, 0, NUMWORDS*NN_DIGIT_LEN);

    memset(PartialPrivA, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(PartialPrivB, 0, NUMWORDS*NN_DIGIT_LEN);

    memset(l_A, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(W, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(T, 0, NUMWORDS*NN_DIGIT_LEN);


    //initialize all x and y points of all public keys to zero
    memset(PubKeyKGC.x, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(PubKeyKGC.y, 0, NUMWORDS*NN_DIGIT_LEN);

    memset(PartialPubKGC.x, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(PartialPubKGC.y, 0, NUMWORDS*NN_DIGIT_LEN);
    
    memset(SecPubKeyA.x, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(SecPubKeyA.y, 0, NUMWORDS*NN_DIGIT_LEN);
    
    memset(SecPubKeyB.x, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(SecPubKeyB.y, 0, NUMWORDS*NN_DIGIT_LEN);
    
    memset(FullPubKeyA.x, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(FullPubKeyA.y, 0, NUMWORDS*NN_DIGIT_LEN);
    
    memset(FullPubKeyB.x, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(FullPubKeyB.y, 0, NUMWORDS*NN_DIGIT_LEN);

    memset(U.x, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(U.y, 0, NUMWORDS*NN_DIGIT_LEN);

    //ECC_init();

    param = ECC_get_param();
    get_param(param);
    ECC_init();
}

void displayhex_16(NN_DIGIT *value){
    int i;
    for (i = 0; i < NUMWORDS; i++) {
        printf("%02lX", value[i]);  
    }
    printf("\n");
}

void gen_SecretKey(NN_DIGIT * PrivateKey){
	uint32_t time_a, time_b;
    time_a = clock_time();

    ECC_gen_private_key(PrivateKey);
    time_b = clock_time();

    printf("Generated Secret Key: \n");
    displayhex_16(PrivateKey);

    t = time_b - time_a;
    printf("Time Taken : %u ms\n", (unsigned int)(t*1000/CLOCK_SECOND));
}

void gen_PublicKey(NN_DIGIT * privatekey, Point * PubKey){
    uint32_t time_a, time_b;
	
    time_a = clock_time();
	
    //ECC_gen_public_key(&PubKey, privatekey);
    ECC_mul(PubKey, &(param->G), privatekey);
	
    time_b = clock_time();
    t = time_b - time_a;

    printf("Generated Public Key: \n");
    printf("X value: ");
    displayhex_16(PubKey->x);
    printf("Y value: ");
    displayhex_16(PubKey->y);
	
	printf("Time Taken : %u ms\n", (unsigned int)(t*1000/CLOCK_SECOND));
	printf("\n");
}

void scalar_mult(NN_DIGIT * value, Point * in, Point * out){
    //uint32_t time_a, time_b;
    
    //time_a = clock_time();
    ECC_mul(out, in, value);
    
    //time_b = clock_time();
    //t = time_b - time_a;

    printf("Generated Point Vectors: \n");
    printf("X value: ");
    displayhex_16(out->x);
    printf("Y value: ");
    displayhex_16(out->y);
    
    //printf("Time Taken : %u ms\n", (unsigned int)(t*1000/CLOCK_SECOND));
    //printf("\n");
}

void gen_PartialPrivKGC(){
    uint32_t time_a, time_b;

    time_a = clock_time();
    gen_SecretKey(SecretKGC);
    gen_PublicKey(SecretKGC, &PartialPubKGC);

    time_b = clock_time();
    t = time_b - time_a;
    
    printf("KGC Partial Key Generation\n");
    printf("Time Taken : %u ms\n", (unsigned int)(t*1000/CLOCK_SECOND));
    printf("\n");
}

void gen_FullPrivKey(Point * UserParPub, NN_DIGIT * UserParPriv, NN_DIGIT * UserSecKey, uint8_t * ID)
{
    uint32_t time_a, time_b;

    uint8_t R_x[KEYDIGITS*NN_DIGIT_LEN];
    uint8_t P_x[KEYDIGITS*NN_DIGIT_LEN];
    uint8_t Message_SHA[45];
    uint8_t HashMSG[20];

    NN_DIGIT Hmsg[NUMWORDS];
    NN_DIGIT Mprt[2*NUMWORDS];

    SHA1Context ctx;

    printf("Generating Full Private and Public Keys\r\n");

    //R_A = PartialPubKGC.x
    NNEncode(R_x, KEYDIGITS*NN_DIGIT_LEN, PartialPubKGC.x, NUMWORDS);

    //P_A = SecPubKeyA.x
    NNEncode(P_x, KEYDIGITS*NN_DIGIT_LEN, UserParPub->x, NUMWORDS);

    //Create (ID, R_x, P_x)
    memcpy(Message_SHA, ID, 1);
    memcpy(Message_SHA+1, R_x, KEYDIGITS*NN_DIGIT_LEN);
    memcpy(Message_SHA+23, P_x, KEYDIGITS*NN_DIGIT_LEN);

    time_a = clock_time();
    //Hash the (ID, R_x, P_x)
    SHA1_Reset(&ctx);
    SHA1_Update(&ctx, Message_SHA, 45);
    SHA1_Digest(&ctx, HashMSG);

    NNDecode(Hmsg, NUMWORDS, HashMSG, 20);
    printf("Hashed Message: ");
    displayhex_16(Hmsg);
    printf("\n");

    //Compute d_A = r_A + xH_0(ID_A, R_A, P_A) mod q
    // We substitute H_0 with SHA1
    // x = msk

    //x * H_0(ID, R_x, P_x)
    NNMult(Mprt, msk, Hmsg, NUMWORDS);

    //d_x = r_x + xH_0(ID, R_x, P_x) mod q
    NNModAdd(UserParPriv, SecretKGC, Mprt, param->p, NUMWORDS);

    //Full Private Key sk_A = (d_x + x_x)
    //NNAdd(UserFullPriv, UserParPriv, UserSecKey, NUMWORDS);

    //Full Public Key pk_A = (P_x + R_x)
    //ECC_add(UserFullPub, &UserParPub, &PartialPubKGC);

    time_b = clock_time();
    
    t = time_b - time_a;

    printf("User d_x: ");
    displayhex_16(UserParPriv);

    printf("Time Taken : %u ms\n", (unsigned int)(t*1000/CLOCK_SECOND));
    printf("\r\n");
}

void symKeyGen(){
    uint32_t time_a, time_b;
    uint8_t R_B[KEYDIGITS*NN_DIGIT_LEN];
    uint8_t P_B[KEYDIGITS*NN_DIGIT_LEN];
    uint8_t tempMSG[45];
    uint8_t HashMSG[20];
    SHA1Context ctx, ptx;

    uint8_t U_1[KEYDIGITS*NN_DIGIT_LEN];
    uint8_t T_1[KEYDIGITS*NN_DIGIT_LEN];
    uint8_t LPB[KEYDIGITS*NN_DIGIT_LEN];
    uint8_t tempHash[89];
    uint8_t KHash[20];

    NN_DIGIT Hmsg[NUMWORDS];

    int chk;

    Point scalar_out1;
    Point scalar_out2;
    Point scalar_out3;
    Point lpb;

    printf("Generating Symmetric Key for User\r\n");

    memset(scalar_out1.x, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(scalar_out1.y, 0, NUMWORDS*NN_DIGIT_LEN);

    memset(scalar_out2.x, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(scalar_out2.y, 0, NUMWORDS*NN_DIGIT_LEN);

    memset(scalar_out3.x, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(scalar_out3.y, 0, NUMWORDS*NN_DIGIT_LEN);

    memset(lpb.x, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(lpb.y, 0, NUMWORDS*NN_DIGIT_LEN);
    
    time_a = clock_time();
    
    // Call the ECC function to generate a random secret value 
    // l_A
    gen_SecretKey(l_A);

    // Compute the public key by scalar multiplication
    // U = l_A * P
    gen_PublicKey(l_A, &U);

    // T = l_A * H_0(ID_B, R_B, P_B)P_PUB + l_A * R_B mod q
    // l_A * H_0(ID_B, R_B, P_B)P_PUB

    //Encode to uint8_t for Hashing
    // R_B = PartialPubKGC.x
    NNEncode(R_B, KEYDIGITS*NN_DIGIT_LEN, PartialPubKGC.x, NUMWORDS);
    // P_B = SecPubKeyB.x
    NNEncode(P_B, KEYDIGITS*NN_DIGIT_LEN, SecPubKeyB.x, NUMWORDS);

    // Create (ID_B, R_B, P_B)
    memcpy(tempMSG, &B_ID, 1);
    memcpy(tempMSG+1, R_B, KEYDIGITS*NN_DIGIT_LEN);
    memcpy(tempMSG+23, P_B, KEYDIGITS*NN_DIGIT_LEN);

    // Hash the (ID_B, R_B, P_B)
    SHA1_Reset(&ctx);
    SHA1_Update(&ctx, tempMSG, 45);
    SHA1_Digest(&ctx, HashMSG);

    //Decode back to uint16_t
    NNDecode(Hmsg, NUMWORDS, HashMSG, 20);
    printf("Hashed Message: ");
    displayhex_16(Hmsg);

    //Using scalar multiplication btn a point and a number
    // H_0(ID_B, R_B, P_B)*P_PUB
    scalar_mult(Hmsg, &PubKeyKGC, &scalar_out1);
    // l_A * H_0(ID_B, R_B, P_B)P_PUB
    scalar_mult(l_A, &scalar_out1, &scalar_out2);
    // l_A * R_B
    scalar_mult(l_A, &PartialPubKGC, &scalar_out3);
    
    // T = l_A * H_0(ID_B, R_B, P_B)P_PUB + l_A * R_B mod q
    NNModAdd(T, scalar_out3.x, scalar_out2.x, param->p, NUMWORDS);  

    //l_A * P_B
    scalar_mult(l_A, &SecPubKeyB, &lpb);

    //Encode U, T, l_A * P_B, ID_B, and P_B to uint8_t for Hashing
    NNEncode(U_1, KEYDIGITS*NN_DIGIT_LEN, U.x, NUMWORDS);
    NNEncode(T_1, KEYDIGITS*NN_DIGIT_LEN, T, NUMWORDS);
    NNEncode(LPB, KEYDIGITS*NN_DIGIT_LEN, lpb.x, NUMWORDS);

    // Create (U, T, l_A * P_B, ID_B, P_B)
    memcpy(tempHash, U_1, KEYDIGITS*NN_DIGIT_LEN);
    memcpy(tempHash+22, T_1, KEYDIGITS*NN_DIGIT_LEN);
    memcpy(tempHash+44, LPB, KEYDIGITS*NN_DIGIT_LEN);
    memcpy(tempHash+66, &B_ID, 1);
    memcpy(tempHash+67, P_B, KEYDIGITS*NN_DIGIT_LEN);

    // K = H_1(U, T, l_A * P_B, ID_B, P_B)
    SHA1_Reset(&ptx);
    SHA1_Update(&ptx, tempHash, 89);
    SHA1_Digest(&ctx, KHash);

    time_b = clock_time();
    t = time_b - time_a;

    memcpy(SecretK1, KHash, 20);
    printf("K1: ");
    for (chk = 0; chk < sizeof(SecretK1); ++chk){
      printf("%02X", KHash[chk]);
    }
    printf("\n");
    //displayHex(SecretK1);

    printf("Time Taken : %u ms\n", (unsigned int)(t*1000/CLOCK_SECOND));
    printf("\n");
  }

void encapsulate(){
    uint32_t time_a, time_b;

    uint8_t P_A[KEYDIGITS*NN_DIGIT_LEN];
    uint8_t P_B[KEYDIGITS*NN_DIGIT_LEN];
    uint8_t U_1[KEYDIGITS*NN_DIGIT_LEN];
    uint8_t T_1[KEYDIGITS*NN_DIGIT_LEN];

    NN_DIGIT W_1[2*NUMWORDS];
    NN_DIGIT W_2[2*NUMWORDS];
    NN_DIGIT W_3[2*NUMWORDS];

    uint8_t tempMSG[90];
    uint8_t HashMSG[20];
    SHA1Context ctx;

    NN_DIGIT H[NUMWORDS];


    printf("Key Encapsulation Process\n");

    //Omitting the ciphertext for now

    //Encoding all values to String
    NNEncode(U_1, KEYDIGITS*NN_DIGIT_LEN, U.x, NUMWORDS);
    NNEncode(T_1, KEYDIGITS*NN_DIGIT_LEN, T, NUMWORDS);
    NNEncode(P_A, KEYDIGITS*NN_DIGIT_LEN, SecPubKeyA.x, NUMWORDS);
    NNEncode(P_B, KEYDIGITS*NN_DIGIT_LEN, SecPubKeyB.x, NUMWORDS);

    //Creating (U, T, ID_A, P_A, ID_B, P_B)
    memcpy(tempMSG, U_1, KEYDIGITS*NN_DIGIT_LEN);
    memcpy(tempMSG+22, T_1, KEYDIGITS*NN_DIGIT_LEN);
    memcpy(tempMSG+44, &A_ID, 1);
    memcpy(tempMSG+45, P_A, KEYDIGITS*NN_DIGIT_LEN);
    memcpy(tempMSG+67, &B_ID, 1);
    memcpy(tempMSG+68, P_B, KEYDIGITS*NN_DIGIT_LEN);

    time_a = clock_time();
    // H = SHA1(U, T, ID_A, P_A, ID_B, P_B)
    SHA1_Reset(&ctx);
    SHA1_Update(&ctx, tempMSG, 90);
    SHA1_Digest(&ctx, HashMSG);

    NNDecode(H, NUMWORDS, HashMSG, 20);

    // Computing W = d_A + l_A * H + x_A * H
    // W_1 = l_A * H
    NNMult(W_1, l_A, H, NUMWORDS);

    // W_2 = x_A * H
    NNMult(W_2, SecretKeyA, H, NUMWORDS);

    // W3 = W_1 + W_2
    NNAdd(W_3, W_2, W_1, 2*NUMWORDS);

    // W = d_A + W3
    NNAdd(W, PartialPrivA, W_3, NUMWORDS);

    time_b = clock_time();
    t = time_b - time_a;

    printf("Encapsulation Complete \n");
    printf("Time Taken : %u ms\n", (unsigned int)(t*1000/CLOCK_SECOND));
    printf("\n");
   
  }

  // ===============================================================================================================================

  void decapsulate(){
    uint32_t time_a, time_b;

    uint8_t P_B[KEYDIGITS*NN_DIGIT_LEN];
    uint8_t U_1[KEYDIGITS*NN_DIGIT_LEN];
    uint8_t T_1[KEYDIGITS*NN_DIGIT_LEN];
    uint8_t xBU[KEYDIGITS*NN_DIGIT_LEN];

    uint8_t tempMSG[89];
    uint8_t HashMSG[20];
    SHA1Context ctx;

    int chk;

    Point x_B;
    Point T_2;
    

    printf("Key Decapsulation Process\r\n");

    memset(x_B.x, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(x_B.y, 0, NUMWORDS*NN_DIGIT_LEN);

    memset(T_2.x, 0, NUMWORDS*NN_DIGIT_LEN);
    memset(T_2.y, 0, NUMWORDS*NN_DIGIT_LEN);

    time_a = clock_time();
    
    // T = d_B * U
    scalar_mult(PartialPrivB, &U, &T_2);

    // x_B = xB * U
    scalar_mult(SecretKeyB, &U, &x_B);

    //Omitting the ciphertext for now

    //Encoding all values to String
    NNEncode(U_1, KEYDIGITS*NN_DIGIT_LEN, U.x, NUMWORDS);
    NNEncode(T_1, KEYDIGITS*NN_DIGIT_LEN, T_2.x, NUMWORDS);
    NNEncode(xBU, KEYDIGITS*NN_DIGIT_LEN, x_B.x, NUMWORDS);
    NNEncode(P_B, KEYDIGITS*NN_DIGIT_LEN, SecPubKeyB.x, NUMWORDS);

    //Creating (U, T, ID_A, P_A, ID_B, P_B)
    memcpy(tempMSG, U_1, KEYDIGITS*NN_DIGIT_LEN);
    memcpy(tempMSG+22, T_1, KEYDIGITS*NN_DIGIT_LEN);
    memcpy(tempMSG+44, xBU, KEYDIGITS*NN_DIGIT_LEN);
    memcpy(tempMSG+66, &B_ID, 1);
    memcpy(tempMSG+67, P_B, KEYDIGITS*NN_DIGIT_LEN);

    // K = H_1(U, T, x_B * U, ID_B, P_B)
    SHA1_Reset(&ctx);
    SHA1_Update(&ctx, tempMSG, 89);
    SHA1_Digest(&ctx, HashMSG);

    time_b = clock_time();
    t = time_b - time_a;

    memcpy(SecretK2, HashMSG, 20);
    printf("K2: ");
    for (chk = 0; chk < sizeof(SecretK2); ++chk){
      printf("%02X", HashMSG[chk]);
    }
    
    printf("Time Taken : %u ms\n", (unsigned int)(t*1000/CLOCK_SECOND));
    printf("\n");
  }

/* scopes process */
PROCESS_THREAD(fitf_process, ev, data)
{
  PROCESS_BEGIN();

  printf("Test Implementation for FitF on Zolertia Remote-RevB\n");

  /* create and start an event timer */
  static struct etimer timer_1;
  etimer_set(&timer_1, 5 * CLOCK_SECOND);
  
  printf("Entering Test Loop.\n");
    
  /* wait till the timer expires and then reset it immediately */
  PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&timer_1));
//    etimer_reset(&tester_timer);
  do {
	  init_data();

      printf("Round %d\n", round_index);

      //Setup by KGC
	  gen_SecretKey(msk);
      gen_PublicKey(msk, &PubKeyKGC);

      //Set Secret Value A
      gen_SecretKey(SecretKeyA);
      gen_PublicKey(SecretKeyA, &SecPubKeyA);

      //Set Secret Value B
      gen_SecretKey(SecretKeyB);
      gen_PublicKey(SecretKeyB, &SecPubKeyB);

	  //Generate Partial and Full Private Key Process
      gen_PartialPrivKGC();
      //For A
      gen_FullPrivKey(&SecPubKeyA, PartialPrivA, SecretKeyA, &A_ID);
	  //For B
      gen_FullPrivKey(&SecPubKeyB, PartialPrivB, SecretKeyB, &B_ID);

      //Symmetric Key Generation
      symKeyGen();

      //Encapsulation and Decapsulation
      encapsulate();
      decapsulate();

	  round_index++;
	  
  } while(round_index < MAX_ROUNDS);

  PROCESS_END();
}
