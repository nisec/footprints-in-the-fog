includes result;
includes ECC;
includes sha1;

configuration FitFAppC
{

}

implementation 
{
  
  components FitFC as App;
  components MainC; 
  components LedsC; 
  components RandomLfsrC;
  components NNM; 
  components ECCC; 
  components ECDHC;
  components SHA1M;
  components secp128r1;

  //For writing into serial port
  components SerialPrintfC;
  
  MainC.SoftwareInit -> RandomLfsrC.Init;
  App.Boot -> MainC.Boot;

  components new TimerMilliC(), LocalTimeMilliC;

  App.myTimer -> TimerMilliC;
  App.LocalTime -> LocalTimeMilliC;
  App.Random -> RandomLfsrC;
  App.Leds -> LedsC;

  components SerialActiveMessageC as Serial;

  App.PriKeyMsg -> Serial.AMSend[AM_PRIVATE_KEY_MSG];
  App.TimeMsg -> Serial.AMSend[AM_TIME_MSG];
  App.SndSecret -> Serial.AMSend[AM_ECDH_KEY_MSG];
  App.SerialControl -> Serial;

  App.NN -> NNM.NN;
  App.ECC -> ECCC.ECC;
  App.ECDH -> ECDHC;
  App.SHA1 -> SHA1M.SHA1;
  App.CurveParam -> secp128r1;
}