/**
 * All new code in this distribution is Copyright 2005 by North Carolina
 * State University. All rights reserved. Redistribution and use in
 * source and binary forms are permitted provided that this entire
 * copyright notice is duplicated in all such copies, and that any
 * documentation, announcements, and other materials related to such
 * distribution and use acknowledge that the software was developed at
 * North Carolina State University, Raleigh, NC. No charge may be made
 * for copies, derivations, or distributions of this material without the
 * express written consent of the copyright holder. Neither the name of
 * the University nor the name of the author may be used to endorse or
 * promote products derived from this material without specific prior
 * written permission.
 *
 * IN NO EVENT SHALL THE NORTH CAROLINA STATE UNIVERSITY BE LIABLE TO ANY
 * PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 * DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 * EVEN IF THE NORTH CAROLINA STATE UNIVERSITY HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN
 * "AS IS" BASIS, AND THE NORTH CAROLINA STATE UNIVERSITY HAS NO
 * OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR
 * MODIFICATIONS. "
 *
 */

/**
 * Module for curve secp160k1
 *
 * Author: An Liu
 * Date: 09/15/2005
 * Modified by: Panos Kampanakis
 * Date: 01/31/2007
 */
#include <ECC.h>
#include <NN.h>
#include <CurveParam.h>

#include <string.h>

void get_param(Params *para)
  {
    memset(para->p, 0, NUMWORDS*NN_DIGIT_LEN);
    para->p[4] = 0xFFFFFFFF;
    para->p[3] = 0xFFFFFFFF;
    para->p[2] = 0xFFFFFFFF;
    para->p[1] = 0xFFFFFFFE;
    para->p[0] = 0xFFFFAC73;

    memset(para->omega, 0, NUMWORDS*NN_DIGIT_LEN);
    para->omega[0] = 0x0000538D;
    para->omega[1] = 0x00000001;

    //cure that will be used
    //a
    memset(para->E.a, 0, NUMWORDS*NN_DIGIT_LEN);
    para->E.a_minus3 = FALSE;
    para->E.a_zero = TRUE;
    
    //b
    memset(para->E.b, 0, NUMWORDS*NN_DIGIT_LEN);
    para->E.b[0] = 0x00000007;

    //base point
    para->G.x[5] = 0x00000000;
    para->G.x[4] = 0x3B4C382C;
    para->G.x[3] = 0xE37AA192;
    para->G.x[2] = 0xA4019E76;
    para->G.x[1] = 0x3036F4F5;
    para->G.x[0] = 0xDD4D7EBB;
    
    para->G.y[5] =  0x00000000;
    para->G.y[4] =  0x938CF935;
    para->G.y[3] =  0x318FDCED;
    para->G.y[2] =  0x6BC28286;
    para->G.y[1] =  0x531733C3;
    para->G.y[0] =  0xF03C4FEE;
    	
    //prime divide the number of points
    para->r[5] = 0x00000001;
    para->r[4] = 0x00000000;
    para->r[3] = 0x00000000;
    para->r[2] = 0x0001B8FA;
    para->r[1] = 0x16DFAB9A;
    para->r[0] = 0xCA16B6B3;
//#endif

  }

NN_UINT omega_mul(NN_DIGIT *a, NN_DIGIT *b, NN_DIGIT *omega, NN_UINT digits) 
{
    a[digits] += NNAddDigitMult(a, a, omega[0], b, digits);
    NNAdd(&a[1], &a[1], b, digits+1);
    return (digits+2);

}

